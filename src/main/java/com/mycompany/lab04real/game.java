/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04real;

/**
 *
 * @author aof2a
 */
import java.util.Scanner;
public class game {
    private player currentPlayer;
    private player playerx;
    private player playero;
    private board Board;
    
    game(){
        playerx = new player('X');
        playero = new player('O');
        Board = new board();
        currentPlayer = playerx;
    }
    public void printWelcome(){
        System.out.println("Welcome to Ox");
    }
    public void printBoard(){
        for(int row = 0 ; row < 3 ;row++ ){
            for(int col = 0; col <3 ;col++){
                System.out.print(Board.getTable(row,col)+" ");
            }
            System.out.println();
        }
    }
    public boolean isValidMove(int row,int col){
        return Board.getTable(row,col)=='-';
    }
    public void placePiece(){
        Scanner kb = new Scanner(System.in);
        
       do {
           System.out.println(currentPlayer.getSymbol()+ " turn");
           System.out.print("Please input row and col (x x):");
    int row = kb.nextInt();
    int col = kb.nextInt();
    
    if (isValidMove(row, col)) {
        Board.setTable(row, col, currentPlayer.getSymbol());
        break;
        } else {
        System.out.println("Invalid move. Try again.");
        }
    } while (true);
    }
    public boolean checkDraw(){
        return Board.isFull();
    }
    
    public boolean checkWin(){
        return checkRow()||checkCol()||checkDia();
    }
    
    public boolean checkRow(){
        for(int i = 0;i<3;i++){
            if(Board.getTable(i, 0)==Board.getTable(i, 1)&&Board.getTable(i, 1)==Board.getTable(i, 2)&&Board.getTable(i, 2)!='-'){
                return true;
            }
        }
        return false;
    }
    public boolean checkCol(){
        for(int i = 0;i<3;i++){
            if(Board.getTable(0, i)==Board.getTable(1, i)&&Board.getTable(1, i)==Board.getTable(2, i)&&Board.getTable(2, i)!='-'){
                return true;
            }
        }
        return false;
    }
    public boolean checkDia(){
        
            if(Board.getTable(0, 0)==Board.getTable(1, 1)&&Board.getTable(1, 1)==Board.getTable(2, 2)&&Board.getTable(2, 2)!='-'){
                return true;
            }else if(Board.getTable(0, 2)==Board.getTable(1, 1)&&Board.getTable(1, 1)==Board.getTable(2, 0)&&Board.getTable(2, 0)!='-'){
                return true;
            }
            else{
                return false;
            }
        
    }
    public void switchPlayer(){
        if(currentPlayer == playerx){
            currentPlayer = playero;
        }else{
            currentPlayer = playerx;
        }
    }
    
    public void printScore(){
        System.out.println("Player X = "+playerx.getScore() + "Player O = "+playero.getScore());
    }
    public void play(){
        printWelcome();
        while(true){
        printBoard();
        placePiece();
        if(checkWin()){
            currentPlayer.incrementScore();
            printBoard();
            System.out.println(currentPlayer.getSymbol() + " win");
             if (playAgain()) { 
                Board.reset(); 
                currentPlayer = playerx;
                
            } else {
                break; 
            }
        }
        else if (checkDraw()) {
            printBoard();
            System.out.println("Draw");
            printScore();
            if (playAgain()) { 
                Board.reset(); 
                currentPlayer = playerx; 
                 
            } else {
                break;
            }
        }
        switchPlayer();
    }
    }   
    public boolean playAgain(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Do you want to play again? (y/n): ");
        String input = kb.next().trim().toLowerCase();
        return input.equals("y");
    }
}


