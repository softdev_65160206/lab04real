/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04real;

/**
 *
 * @author aof2a
 */
public class player {
    private char symbol;
    private int score;
    player(char symbol){
        this.symbol = symbol;
        this.score = 0;
    }
    public char getSymbol(){
        return symbol;
    }
    public int getScore(){
        return score;
    }
    
    public void incrementScore(){
        score++;
    }
}
