/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04real;

/**
 *
 * @author aof2a
 */
public class board {
      private char table[][] = {{'-','-','-'}, {'-', '-', '-'},{'-', '-', '-'}};
      
      public char getTable(int row , int col){
          return table[row][col];
      }
      
      public void setTable(int row, int col,char val){
          table[row][col] = val;
      }
      
      public boolean isFull(){
        for(int row= 0 ; row<3;row++){
          for(int col = 0; col<3; col++){
              if(table[row][col]=='-'){
                  return false;
                }
            }
        }
        return true;
    }
      public void reset(){
          for(int row= 0 ; row<3;row++){
          for(int col = 0; col<3; col++){
              table[col][row]= '-';
            }
        }
      }
      
      
}

